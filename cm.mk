# Boot animation
TARGET_SCREEN_HEIGHT := 1080
TARGET_SCREEN_WIDTH := 1920

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

# Inherit device configuration
$(call inherit-product, device/rockchip/freakbox/full_freakbox.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_freakbox
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
               PRODUCT_NAME=Freakbox_Project \
               BUILD_UTC_DATE=0
